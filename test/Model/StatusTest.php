<?php
/**
 * StatusTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  vdmcworld\client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Vademecum Online API
 *
 * This is the Vademecum API documentation which will allow you to connect to API.
 *
 * OpenAPI spec version: 1.0.0
 * Contact: birce@vademecumonline.com.tr
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 2.3.1
 */

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace vdmcworld\client;

/**
 * StatusTest Class Doc Comment
 *
 * @category    Class */
// * @description Status
/**
 * @package     vdmcworld\client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class StatusTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "Status"
     */
    public function testStatus()
    {
    }

    /**
     * Test attribute "status"
     */
    public function testPropertyStatus()
    {
    }

    /**
     * Test attribute "description"
     */
    public function testPropertyDescription()
    {
    }
}
