# PhpClient
This is the Vademecum API documentation which will allow you to connect to API.

This PHP package is automatically generated by the [Swagger Codegen](https://github.com/swagger-api/swagger-codegen) project:

- API version: 1.0.0
- Build package: io.swagger.codegen.languages.PhpClientCodegen

## Requirements

PHP 5.5 and later

## Installation & Usage
### Composer

To install the bindings via [Composer](http://getcomposer.org/), add the following to `composer.json`:

```
{
  "repositories": [
    {
      "type": "git",
      "url": "https://github.com/vdmcworld/vapi-client.git"
    }
  ],
  "require": {
    "vdmcworld/vapi-client": "*@dev"
  }
}
```

Then run `composer install`

### Manual Installation

Download the files and include `autoload.php`:

```php
    require_once('/path/to/PhpClient/vendor/autoload.php');
```

## Tests

To run the unit tests:

```
composer install
./vendor/bin/phpunit
```

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: apiKey
$config = vdmcworld\client\Configuration::getDefaultConfiguration()->setApiKey('authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = vdmcworld\client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('authorization', 'Bearer');

$apiInstance = new vdmcworld\client\Api\ProductRequestsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$productId = 789; // int | ID of product

try {
    $result = $apiInstance->getProductAllergies($productId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductRequestsApi->getProductAllergies: ', $e->getMessage(), PHP_EOL;
}

?>
```

## Documentation for API Endpoints

All URIs are relative to *https://api-test.vademecumonline.com.tr/v1*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*ProductRequestsApi* | [**getProductAllergies**](docs/Api/ProductRequestsApi.md#getproductallergies) | **GET** /product-allergy/{productId} | Returns product allergies
*ProductRequestsApi* | [**getProductCard**](docs/Api/ProductRequestsApi.md#getproductcard) | **GET** /product-card/{productId} | Returns a product card
*ProductRequestsApi* | [**getProductCautionCard**](docs/Api/ProductRequestsApi.md#getproductcautioncard) | **GET** /product-caution-card/{productId} | Returns a product caution card


## Documentation For Models

 - [AllergicReaction](docs/Model/AllergicReaction.md)
 - [Allergy](docs/Model/Allergy.md)
 - [AllergyInfo](docs/Model/AllergyInfo.md)
 - [Atc](docs/Model/Atc.md)
 - [AtcIndex](docs/Model/AtcIndex.md)
 - [Company](docs/Model/Company.md)
 - [ErrorDetails](docs/Model/ErrorDetails.md)
 - [File](docs/Model/File.md)
 - [NfcCode](docs/Model/NfcCode.md)
 - [PrescriptionType](docs/Model/PrescriptionType.md)
 - [Product](docs/Model/Product.md)
 - [ProductAllergy](docs/Model/ProductAllergy.md)
 - [ProductCard](docs/Model/ProductCard.md)
 - [ProductCardCard](docs/Model/ProductCardCard.md)
 - [ProductCardResponse](docs/Model/ProductCardResponse.md)
 - [ProductCardSpecialInformation](docs/Model/ProductCardSpecialInformation.md)
 - [Status](docs/Model/Status.md)
 - [Substance](docs/Model/Substance.md)


## Documentation For Authorization


## apiKey

- **Type**: API key
- **API key parameter name**: authorization
- **Location**: HTTP header


## Author

birce@vademecumonline.com.tr


