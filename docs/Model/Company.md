# Company

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**name** | **string** |  | [optional] 
**officialName** | **string** |  | [optional] 
**website** | **string** |  | [optional] 
**fax** | **string** |  | [optional] 
**phone** | **string** |  | [optional] 
**address** | **string** |  | [optional] 
**email** | **string** |  | [optional] 
**city** | **string** |  | [optional] 
**district** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


