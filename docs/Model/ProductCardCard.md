# ProductCardCard

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**nfcCode** | [**\vdmcworld\client\Model\NfcCode**](NfcCode.md) |  | [optional] 
**prescriptionType** | [**\vdmcworld\client\Model\PrescriptionType**](PrescriptionType.md) |  | [optional] 
**equivalentGroups** | **string[]** |  | [optional] 
**reimbursementStatus** | [**\vdmcworld\client\Model\Status**](Status.md) |  | [optional] 
**specialInformation** | [**\vdmcworld\client\Model\ProductCardSpecialInformation**](ProductCardSpecialInformation.md) |  | [optional] 
**sgkEquivalentCodes** | **string[]** |  | [optional] 
**sgkPriceReferenceCodes** | **string[]** |  | [optional] 
**company** | [**\vdmcworld\client\Model\Company**](Company.md) |  | [optional] 
**publicNumber** | **string** |  | [optional] 
**atcIndices** | [**\vdmcworld\client\Model\AtcIndex[]**](AtcIndex.md) |  | [optional] 
**images** | [**\vdmcworld\client\Model\File[]**](File.md) |  | [optional] 
**shelfLife** | **string** |  | [optional] 
**notice** | **string** |  | [optional] [default to 'null']

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


