# Allergy

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**substance** | [**\vdmcworld\client\Model\Substance**](Substance.md) |  | [optional] 
**substanceAllergyInfo** | [**\vdmcworld\client\Model\AllergyInfo**](AllergyInfo.md) |  | [optional] 
**allergicReactions** | [**\vdmcworld\client\Model\AllergicReaction[]**](AllergicReaction.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


