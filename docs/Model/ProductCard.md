# ProductCard

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product** | [**\vdmcworld\client\Model\Product**](Product.md) |  | [optional] 
**card** | [**\vdmcworld\client\Model\ProductCardCard**](ProductCardCard.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


