# ProductCardResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**success** | **bool** |  | [optional] 
**data** | [**\vdmcworld\client\Model\ProductCard**](ProductCard.md) |  | [optional] 
**error** | [**\vdmcworld\client\Model\ErrorDetails**](ErrorDetails.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


