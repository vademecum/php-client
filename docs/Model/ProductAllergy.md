# ProductAllergy

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product** | [**\vdmcworld\client\Model\Product**](Product.md) |  | [optional] 
**allergies** | [**\vdmcworld\client\Model\Allergy[]**](Allergy.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


