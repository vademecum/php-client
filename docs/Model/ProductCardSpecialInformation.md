# ProductCardSpecialInformation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pediatricStatus** | [**\vdmcworld\client\Model\Status**](Status.md) |  | [optional] 
**emergencyPediatricStatus** | [**\vdmcworld\client\Model\Status**](Status.md) |  | [optional] 
**lightProtectionStatus** | [**\vdmcworld\client\Model\Status**](Status.md) |  | [optional] 
**moistureProtectionStatus** | [**\vdmcworld\client\Model\Status**](Status.md) |  | [optional] 
**coldChainStatus** | [**\vdmcworld\client\Model\Status**](Status.md) |  | [optional] 
**cytotoxicStatus** | [**\vdmcworld\client\Model\Status**](Status.md) |  | [optional] 
**additionalMonitoringStatus** | [**\vdmcworld\client\Model\Status**](Status.md) |  | [optional] 
**orderedDistributionStatus** | [**\vdmcworld\client\Model\Status**](Status.md) |  | [optional] 
**mandatoryChemoDrugStatus** | [**\vdmcworld\client\Model\Status**](Status.md) |  | [optional] 
**dailyTreatmentStatus** | [**\vdmcworld\client\Model\Status**](Status.md) |  | [optional] 
**indicationCompatibilityStatus** | [**\vdmcworld\client\Model\Status**](Status.md) |  | [optional] 
**offLabelUsageStatus** | [**\vdmcworld\client\Model\Status**](Status.md) |  | [optional] 
**hospitalProductStatus** | [**\vdmcworld\client\Model\Status**](Status.md) |  | [optional] 
**dualPricedProductStatus** | [**\vdmcworld\client\Model\Status**](Status.md) |  | [optional] 
**currentExchangeRateProductStatus** | [**\vdmcworld\client\Model\Status**](Status.md) |  | [optional] 
**orphanDrugStatus** | [**\vdmcworld\client\Model\Status**](Status.md) |  | [optional] 
**patientApprovalFormStatus** | [**\vdmcworld\client\Model\Status**](Status.md) |  | [optional] 
**drugSafetyMonitoringFormStatus** | [**\vdmcworld\client\Model\Status**](Status.md) |  | [optional] 
**ceCertificationStatus** | [**\vdmcworld\client\Model\Status**](Status.md) |  | [optional] 
**highRiskStatus** | [**\vdmcworld\client\Model\Status**](Status.md) |  | [optional] 
**freeMarketPriceStatus** | [**\vdmcworld\client\Model\Status**](Status.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


