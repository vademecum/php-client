# vdmcworld\client\ProductRequestsApi

All URIs are relative to *https://api-test.vademecumonline.com.tr/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getProductAllergies**](ProductRequestsApi.md#getProductAllergies) | **GET** /product-allergy/{productId} | Returns product allergies
[**getProductCard**](ProductRequestsApi.md#getProductCard) | **GET** /product-card/{productId} | Returns a product card
[**getProductCautionCard**](ProductRequestsApi.md#getProductCautionCard) | **GET** /product-caution-card/{productId} | Returns a product caution card


# **getProductAllergies**
> \vdmcworld\client\Model\ProductAllergy getProductAllergies($productId)

Returns product allergies



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: apiKey
$config = vdmcworld\client\Configuration::getDefaultConfiguration()->setApiKey('authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = vdmcworld\client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('authorization', 'Bearer');

$apiInstance = new vdmcworld\client\Api\ProductRequestsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$productId = 789; // int | ID of product

try {
    $result = $apiInstance->getProductAllergies($productId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductRequestsApi->getProductAllergies: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productId** | **int**| ID of product |

### Return type

[**\vdmcworld\client\Model\ProductAllergy**](../Model/ProductAllergy.md)

### Authorization

[apiKey](../../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getProductCard**
> \vdmcworld\client\Model\ProductCardResponse getProductCard($productId)

Returns a product card



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: apiKey
$config = vdmcworld\client\Configuration::getDefaultConfiguration()->setApiKey('authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = vdmcworld\client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('authorization', 'Bearer');

$apiInstance = new vdmcworld\client\Api\ProductRequestsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$productId = 789; // int | ID of product

try {
    $result = $apiInstance->getProductCard($productId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductRequestsApi->getProductCard: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productId** | **int**| ID of product |

### Return type

[**\vdmcworld\client\Model\ProductCardResponse**](../Model/ProductCardResponse.md)

### Authorization

[apiKey](../../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getProductCautionCard**
> \vdmcworld\client\Model\ProductCardResponse getProductCautionCard($productId)

Returns a product caution card



### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

// Configure API key authorization: apiKey
$config = vdmcworld\client\Configuration::getDefaultConfiguration()->setApiKey('authorization', 'YOUR_API_KEY');
// Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
// $config = vdmcworld\client\Configuration::getDefaultConfiguration()->setApiKeyPrefix('authorization', 'Bearer');

$apiInstance = new vdmcworld\client\Api\ProductRequestsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$productId = 789; // int | ID of product

try {
    $result = $apiInstance->getProductCautionCard($productId);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProductRequestsApi->getProductCautionCard: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productId** | **int**| ID of product |

### Return type

[**\vdmcworld\client\Model\ProductCardResponse**](../Model/ProductCardResponse.md)

### Authorization

[apiKey](../../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

